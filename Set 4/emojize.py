import emoji


def get_user_input():
    s = input('Gib input: ')
    return s


def print_with_emojis(string):
    print(emoji.emojize(string))


def main():
    text = get_user_input()
    print_with_emojis(text)


main()
