import random

LEVELS = {
    1: (1, 9),
    2: (10, 99),
    3: (100, 999)
}

ERROR_MSG = 'EEE'
GEUSS_COUNT = 3


def get_level():
    while True:
        try:
            level = int(input('Level: '))
            if level in range(1, 4):
                return level
        except ValueError:
            continue


def generate_integer(level):
    number_one = random.randint(LEVELS[level][0], LEVELS[level][1])
    number_two = random.randint(LEVELS[level][0], LEVELS[level][1])
    return number_one, number_two


def get_guess(result, num1, num2):
    global GEUSS_COUNT
    while GEUSS_COUNT > 0:
        try:
            user_guess = int(input())
            if user_guess == result:
                return True
            else:
                GEUSS_COUNT -= 1
                print(ERROR_MSG)
                if GEUSS_COUNT != 0:
                    print(f'{num1} + {num2} = ', end='')
        except ValueError:
            print(f'{num1} + {num2} = ', end='')

    print(f'{num1} + {num2} = {result}')
    return False


def main():
    points = 0
    count = 5
    level = get_level()
    while count > 0:
        num1, num2 = generate_integer(level)
        result = num1 + num2
        print(f'{num1} + {num2} = ', end='')
        if get_guess(result, num1, num2):
            points += 1
            count -= 1
            continue
        else:
            count -= 1
    print(f'Your score is: {points}')


main()
