import requests
import sys


def get_price(amount, price):
    return amount * price


try:
    response = requests.get(
        'https://api.coindesk.com/v1/bpi/currentprice.json')
    price = response.json()['bpi']['USD']['rate_float']
except requests.RequestException:
    print('Something went wrong with request')


def main():
    if len(sys.argv) < 2 or sys.argv[1].isalpha():
        print('Missing command-line argument or command is not an integer')
        sys.exit()
    amount = float(sys.argv[1])
    print(f'${get_price(amount, price):,.4f}')


main()
