def get_userinput():
    expression = input('Gib expression: ')
    x, y, z = expression.split(' ')
    return float(x), y, float(z)


def multiply(x, z):
    return x * z


def add(x, z):
    return x + z


def subtract(x, z):
    return x - z


def divide(x, z):
    return x / z


def main():
    while True:
        x, y, z = get_userinput()
        match y:
            case '*':
                print(multiply(x, z))
            case '+':
                print(add(x, z))
            case '-':
                print(subtract(x, z))
            case '/':
                print(divide(x, z))
            case _:
                print('something went wrong...')


main()
