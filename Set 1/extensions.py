dict = {
    'gif': 'image/gif',
    'jpg': 'image/jpeg',
    'jpeg': 'image/jpeg',
    'png': 'image/png',
    'pdf': 'application/pdf',
    'txt': 'text/plain',
    'zip': 'application/zip'
}


def get_user_input():
    user_input = input('File name: ')
    return user_input


def media_type(answer):
    answer = answer.split('.')
    return dict[answer[-1]]


def main():
    while True:
        answer = get_user_input()
        print(media_type(answer))


main()
