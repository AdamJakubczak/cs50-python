def get_user_input():
    user_answer = input('Greeting: ')
    user_answer = user_answer.lstrip().lower()
    return user_answer


def check_respone(answer):
    if answer.startswith('hello'):
        return '0$'
    elif answer[0] == 'h':
        return '20$'
    else:
        return '100$'


def main():
    while True:
        answer = get_user_input()
        print(check_respone(answer))


main()
