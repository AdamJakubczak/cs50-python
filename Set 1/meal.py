def get_user_input():
    time = input('What time is it: ')
    return time


def convert(time):
    hour, minute = time.split(':')
    minute = int(minute) / 60
    full_time = int(hour) + minute
    return full_time


def meal_time(full_time):
    if 7 <= full_time <= 8:
        return 'Breakfast time'
    elif 12 <= full_time <= 13:
        return 'Lunch time'
    elif 18 <= full_time <= 19:
        return 'Dinner time'
    else:
        return None


def main():
    while True:
        time = get_user_input()
        full_time = convert(time)
        print(meal_time(full_time))


main()
