from vanity import is_valid


def test_for_special_characters():
    assert not is_valid(',,,,,')
    assert not is_valid('AB00!!')
    assert not is_valid('AB.044')
    assert is_valid('CS50')


def test_lenght():
    assert is_valid('AA50')
    assert not is_valid('C')
    assert not is_valid('AAAAAAAAAAA')


def test_starts_with_two_letters():
    assert not is_valid('A500')
    assert not is_valid('50AA')
    assert is_valid('AA')


def test_numbers_in_the_middle():
    assert not is_valid('AAA22A')
    assert not is_valid('BC56AB')
    assert is_valid('AB5000')


def test_if_first_number_equals_zero():
    assert not is_valid('AA05')
    assert not is_valid('AA00')
    assert is_valid('AA50')
    assert is_valid('AA200')
