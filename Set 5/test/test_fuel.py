import pytest
from fuel import convert, gauge


def test_zerodiverror():
    with pytest.raises(ZeroDivisionError):
        convert('3/0')


def test_valueerror():
    with pytest.raises(ValueError):
        convert('4/3')
    with pytest.raises(ValueError):
        convert('cat/dog')


def test_gauge_E_F():
    assert gauge(0) == 'E'
    assert gauge(1) == 'E'
    assert gauge(99) == 'F'
    assert gauge(100) == 'F'


def test_gauge_numbers():
    assert gauge(50) == '50%'
    assert gauge(10) == '10%'
