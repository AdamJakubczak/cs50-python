from twttr import remove_vowels


def test_strings():
    assert remove_vowels('Twitter') == 'Twttr'
    assert remove_vowels('AEIOU') == ''
    assert remove_vowels('BCDGF') == 'BCDGF'


def test_upper():
    assert remove_vowels('AAAAAA') == ''
    assert remove_vowels('BBBBBB') == 'BBBBBB'


def test_lower():
    assert remove_vowels('aaaaaa') == ''
    assert remove_vowels('bbbbbb') == 'bbbbbb'
