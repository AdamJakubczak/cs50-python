from bank import check_respone


def test_starts_with_hello():
    assert check_respone('Hello') == '0$'
    assert check_respone('Hello good lord') == '0$'
    assert check_respone('   hello :)') == '0$'


def test_starts_with_letter_h():
    assert check_respone('hi') == '20$'
    assert check_respone('hey ho') == '20$'


def test_other_inputs():
    assert check_respone('asdasdasd') == '100$'
    assert check_respone('asdads hello asdasd') == '100$'
