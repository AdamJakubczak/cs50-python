def convert(fraction: str):
    x, y = map(int, fraction.split(sep='/'))
    if y == 0:
        raise ZeroDivisionError
    elif x > y:
        raise ValueError
    percentage = (x / y) * 100
    return round(percentage)


def gauge(percentage):
    if percentage <= 1:
        return 'E'
    elif percentage >= 99:
        return 'F'
    else:
        return f'{percentage}%'


def main():
    fraction = input('Gib fraction')
    percentage = convert(fraction)
    result = gauge(percentage)
    print(result)


if __name__ == "__main__":
    main()
