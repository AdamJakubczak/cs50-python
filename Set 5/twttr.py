def get_user_input():
    phrase = input('Gib phrase: ')
    return phrase


def remove_vowels(phrase):
    shorter = ''
    for letter in phrase:
        if letter.upper() not in ['A', 'E', 'I', 'O', 'U']:
            shorter += letter
    return shorter


def main():
    phrase = get_user_input()
    print(remove_vowels(phrase))


if __name__ == "__main__":
    main()
