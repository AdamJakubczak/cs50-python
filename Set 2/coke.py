amount = 50

while amount > 0:
    print(f'Amount due: {amount}')
    coins = int(input('Insert coin: '))
    if coins not in [5, 10, 15, 25, 50]:
        continue
    else:
        amount -= coins
print(f'Change owned: {abs(amount)}')
