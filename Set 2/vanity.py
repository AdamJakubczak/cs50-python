
def is_valid(plate) -> bool:
    if not plate[:2].isalpha():
        return False
    elif not 2 <= len(plate) <= 6:
        return False
    elif check_for_special_characters(plate):
        return False
    elif check_middle_zero(plate):
        return False
    elif check_if_numbers_come_at_the_end(plate):
        return False
    else:
        return True


def check_for_special_characters(string):
    for letter in string:
        if not letter.isalnum():
            return True
    return False


def check_middle_zero(string):
    numbers_to_check = ''
    for i in string:
        if i.isdigit():
            numbers_to_check += i
    if numbers_to_check[0] == '0':
        return True
    else:
        return False


def check_if_numbers_come_at_the_end(string):
    for i in range(len(string) - 1):
        if string[i].isdigit() and string[i + 1].isalpha():
            return True
    return False


def main():
    while True:
        plate = input('Plate: ')
        if is_valid(plate):
            print('Valid')
        else:
            print('Invalid')


# main()
