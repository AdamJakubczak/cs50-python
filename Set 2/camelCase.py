def get_user_input():
    camel_case = input('Camel case: ')
    return camel_case


def go_snake_case(txt):
    new_word = ''
    for x in txt:
        if x.isupper():
            new_word += f'_{x.lower()}'
        else:
            new_word += x
    return new_word


def main():
    txt = get_user_input()
    print(f'snake_case: {go_snake_case(txt)}')


main()
