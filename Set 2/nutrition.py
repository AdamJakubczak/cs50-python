callories_table = {
    'Apple': 130,
    'Avocado': 50,
    'Banana': 110,
    'Cantaloupe': 50,
    'Grapefruit': 60,
    'Grapes': 90,
    'Honeydew Melon': 50,
    'Kiwifriut': 90,
    'Lemon': 15,
    'Lime': 20,
    'Nectarine': 60,
    'Orange': 80,
    'Peach': 60,
    'Pear': 100,
    'Pineapple': 50,
    'Plum': 70,
    'Strawberry': 50,
    'Sweet Cherries': 100,
    'Tangerine': 50,
    'Watermelon': 80,
}


def get_user_input():
    fruit = input('Gib fruit:')
    return fruit.title()


def get_calories(fruit):
    return callories_table.get(fruit, '')


def main():
    fruit = get_user_input()
    print(f'Callories: {get_calories(fruit)}')


main()
