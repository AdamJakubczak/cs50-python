def get_user_inputs():
    userInput = str(input('Podaj hasło: '))
    return userInput


def slow_it_down(userInput):
    print(userInput.replace(" ", "..."))


def main():
    userInput = get_user_inputs()
    slow_it_down(userInput)


main()
