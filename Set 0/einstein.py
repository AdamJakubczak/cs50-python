def get_user_input():
    while True:
        m = input('Podaj m: ')
        if m.isdigit():
            return m
        else:
            print('Liczba')
            continue


def calculate(m):
    e = int(m) * pow(300000000, 2)
    return e


def main():
    m = get_user_input()
    print(calculate(m))


main()
