def get_user_input():
    userInput = str(input('Gib input: '))
    return userInput


def transform(userInput):
    txt = userInput.replace(':)', '🙂')
    txt = txt.replace(':(', '🙁')
    return txt


def main():
    userInput = get_user_input()
    print(transform(userInput))


main()
