import validators


def main():
    print(validate(input('Gib email to validate: ')))


def validate(s: str) -> str:
    if validators.email(s):
        return 'Valid'
    else:
        return 'Invalid'


if __name__ == '__main__':
    main()
