import re
import sys


class YouTubeLinkGetter():
    def __init__(self) -> None:
        self.regex_pattern = r'https?:\/{2}(?:www\.)?youtube\.com\/embed\/(\w+)'

    def parse_link(self, link):
        parse_match = re.search(self.regex_pattern, link)
        if parse_match:
            return f'https://youtu.be/{parse_match.group(1)}'
        else:
            return None


def main():
    parser = YouTubeLinkGetter()
    print(parser.parse_link(input('Gib link: ')))


if __name__ == '__main__':
    main()
