import re
import sys


def main():
    print(convert(input("Hours: ")))


def convert(s: str):
    time_to_convert = get_time_to_convert(s)
    time_from, time_to = get_times_from_to(time_to_convert)
    converted_from = magical_time_converter(time_from)
    converted_to = magical_time_converter(time_to)
    return f'{converted_from} to {converted_to}'


def get_time_to_convert(s: str) -> list[str]:
    pattern = r'^([1-9]|1[0-2])\:?([0-5][0-9])?\ (AM|PM)\ to\ ([1-9]|1[0-2])\:?([0-5][0-9])?\ (AM|PM)$'
    if match := re.search(pattern, s):
        time_list = [n if n else '00' for n in match.groups()]
        return time_list
    else:
        raise ValueError


def get_times_from_to(time_to_convert):
    time_from = time_to_convert[0:3]
    time_to = time_to_convert[3:6]
    return time_from, time_to


def magical_time_converter(time):
    if time[2] == 'AM':
        if time[0] == '12':
            converted_from_time = '00:' + time[1]
        else:
            converted_from_time = f'{int(time[0]):02}:{time[1]}'
    if time[2] == 'PM':
        converted_from_time = f'{(int(time[0]) + 12):02}:{time[1]}'
    return converted_from_time


if __name__ == '__main__':
    main()
