from um import count


def test_empty_string():
    assert count('') == 0


def test_numbers():
    assert count('000011111000001023123') == 0


def test_multiple_ums():
    assert count('um um um um') == 4
    assert count('umumumumumumum') == 0


def test_um_in_word():
    assert count('drum cum mum bum tadum') == 0


def test_with_characters():
    assert count('um? um! um... um, ,um um. .um') == 7
