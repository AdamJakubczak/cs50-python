import pytest
from working import convert


def test_empty_string():
    with pytest.raises(ValueError):
        convert('')


def test_invalid_string():
    with pytest.raises(ValueError):
        convert('From 9 AM - 5 AM')
    with pytest.raises(ValueError):
        convert('asdasdasdasd')


def test_invalid_hour():
    with pytest.raises(ValueError):
        convert('9:67 AM to 12:00 AM')
    with pytest.raises(ValueError):
        convert('99 AM to 110 PM')


def test_valid_string():
    assert convert('9 AM to 5 PM') == '09:00 to 17:00'
    assert convert('12 AM to 12 PM') == '00:00 to 24:00'
    assert convert('12:00 AM to 12:00 PM') == '00:00 to 24:00'
    assert convert('9:00 AM to 5:00 PM') == '09:00 to 17:00'
