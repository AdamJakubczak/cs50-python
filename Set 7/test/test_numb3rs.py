from numb3rs import validate


def test_alpha():
    assert validate('a.a.a.a') == 'Invalid'
    assert validate('a.b.c.d') == 'Invalid'


def test_characters():
    assert validate('$.$.$.$') == 'Invalid'
    assert validate('23.@@@.@@.@@') == 'Invalid'


def test_negative():
    assert validate('-1.0.-2.-1') == 'Invalid'


def test_lenght():
    assert validate('0.0.0') == 'Invalid'
    assert validate('1.1.1.1.1') == 'Invalid'


def test_out_of_range():
    assert validate('001.295.223.232') == 'Invalid'
    assert validate('255.256.257.259') == 'Invalid'


def test_empty_string():
    assert validate('') == 'Invalid'


def test_correct():
    assert validate('192.168.12.10') == 'Valid'
