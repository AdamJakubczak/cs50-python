def main():
    n = int(input('What is n: '))
    for s in sheep(n):
        print(s)


def sheep(n):
    flock = []
    for i in range(n):
        flock.append('🐑' * i)
    return flock


def add_new_line(start, end):
    ...


def add_another(start, end):
    ...


if __name__ == '__main__':
    main()