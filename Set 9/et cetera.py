#SET Datatype - set does not contain any duplicates!

students = [
    {'name' : 'Hermione', 'house' : 'Gryffindor'},
    {'name' : 'Harry', 'house' : 'Gryffindor'},
    {'name' : 'Ron', 'house' : 'Gryffindor'},
    {'name' : 'Draco', 'house' : 'Slytherin'},
    {'name' : 'Padma', 'house' : 'Ravenclaw'}
]

houses = set()

for student in students:
    houses.add(student['house'])
        
for house in sorted(houses):
    print(house)
    
#GLOBAL VARIABLE

balance = 0

def main():
    print(f'Balance:', balance)
    deposit(100)
    withdraw(50)
    print('Balance:', balance)
    
def deposit(amount):
    global balance
    balance += amount

def withdraw(amount):
    global balance
    balance -= amount
    
main()

# GLOBAL VARIABLE OOP EXAMPLE - no need to use global keyword. Instance variables are accessible for all methods inside the class.

class Bank:
    def __init__(self):
        self._balance = 0
    
    @property
    def balance(self):
        return self._balance
    
    def deposit(self, amount):
        self._balance += amount
    
    def withdraw(self, amount):
        self._balance -= amount

def main():
    bank = Bank()
    print('Balance: ', bank.balance)
    bank.deposit(100)
    print('Balance: ', bank.balance)
    bank.withdraw(10)
    print('Balance: ', bank.balance)

if __name__ == "__main__":
    main()

# CONSTANTS 

MEOWS = 3

for _ in range(MEOWS):
    print('meow')
    
# CONSTANTS For classes

class Cat:
    MEOWS = 3
    
    def meow(self):
        for _ in range(self.MEOWS):
            print('meow')

cat = Cat()

cat.meow()

# TYPE HINTS - use mypy module to check type hints. Run mypy yourfile.py in terminal. DOCSTRING DOCUMENTING

def meow(n: int) -> None:
    '''
    Meow n times.
    
    :param n: Number of times to meow.
    :type n: int
    :raise TypeError: In n is not int
    :return: A string of n meows
    :rtype: str
    '''
    for _ in range(n):
        print('Meow')
        
number = int(input('Gib number: '))
meow(number)

# ARGPARSE module - for parsing arguments from command line. Using type=int it automaticaly converts args.n into Int.

import argparse

parser = argparse.ArgumentParser(description='Meow like a cat')
parser.add_argument('-n', default=1, help='number of times to meow', type=int)
args = parser.parse_args()

for _ in range(args.n):
    print('meow')
    
# UNPACKING values with '*' look at *coins in print(total)

def total(galleons, sickles, knuts):
    return (galleons * 17 + sickles) * 29 + knuts

coins = [100, 50, 25]

print(total(*coins), 'Knuts')

# USE double '**' to unpack dictionaries. It unpacks it as keyword arguments totat(galleons = 100, knuts = 25, sickles = 50)

def total(galleons, sickles, knuts):
    return (galleons * 17 + sickles) * 29 + knuts

coins = {
    'galleons' : 100,
    'knuts' : 25,
    'sickles' : 50    
}

print(total(**coins), 'Knuts')

# *args, **kwargs

def f(*args, **kwargs):
    print('Named: ', kwargs)
    print('Positional', args)

f(5,10,15, galeons = 100, sickles = 50, knuts = 25)

# -----

def main():
    yell('this', 'is', 'cs50')

def yell(*words):
    uppercased = []
    for word in words:
        uppercased.append(word.upper())
    print(*uppercased)

if __name__ == '__main__':
    main()


 #MAP function, applies function to iterable

def main():
    yell('this', 'is', 'cs50')

def yell(*words):
    uppercased = map(str.upper, words)
    print(*uppercased)

if __name__ == '__main__':
    main()
    
#List comprehension

def main():
    yell('this', 'is', 'cs50')

def yell(*words):
    print(*(i.upper() for i in words))

if __name__ == '__main__':
    main()

# List comprehension with IF statement

students = [
    {'name' : 'Hermione', 'house' : 'Gryffindor'},
    {'name' : 'Harry', 'house' : 'Gryffindor'},
    {'name' : 'Ron', 'house' : 'Gryffindor'},
    {'name' : 'Draco', 'house' : 'Slytherin'},
    {'name' : 'Padma', 'house' : 'Ravenclaw'}
]

gryffindors = [student['name'] for student in students if student['house'] == 'Gryffindor']

for g in sorted(gryffindors):
    print(g)
    
students = ['Hermione', 'Harry', 'Ron']

gryffindors = [{'name': student, 'house': 'Gryffindor'} for student in students]

print(gryffindors)
    
# FILTER function - Pass function which acts as filter and apply it to iterabale. 

students = [
    {'name' : 'Hermione', 'house' : 'Gryffindor'},
    {'name' : 'Harry', 'house' : 'Gryffindor'},
    {'name' : 'Ron', 'house' : 'Gryffindor'},
    {'name' : 'Draco', 'house' : 'Slytherin'},
    {'name' : 'Padma', 'house' : 'Ravenclaw'}
]

def is_gryffindor(s):
    return s['house'] == 'Gryffindor'

gryffindors = filter(lambda student: student['house'] == 'Gryffindor', students)

for g in gryffindors:
    print(g)
    
# DICTIONARY COMPREHENSION

students = ['Hermione', 'Harry', 'Ron']

gryffindors = {student: 'Gryffindor' for student in students}

print(gryffindors)


# GENERATORS - trying to return 10000000 of those sheeps You ran out of CPU...

def main():
    n = int(input('What is n: '))
    for s in sheep(n):
        print(s)


def sheep(n):
    flock = []
    for i in range(n):
        flock.append('🐑' * i)
    return flock


if __name__ == '__main__':
    main()
    
#YIELD - You use yield instead to return data piece by piece.

def main():
    n = int(input('What is n: '))
    for s in sheep(n):
        print(s)


def sheep(n):
    for i in range(n):
        yield '🐑' * i
 

if __name__ == '__main__':
    main()