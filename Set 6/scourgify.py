import csv
import sys


def get_file_name() -> tuple:
    if len(sys.argv) != 3:
        print('Wrong amout of command line arguments')
        sys.exit()
    else:
        file1, file2 = sys.argv[1:3]
        return file1, file2


def check_file_format(file1: str, file2: str) -> bool:
    if file1.endswith('.csv') and file2.endswith('.csv'):
        return True
    else:
        print('Wrong file format')
        sys.exit()


def get_names_and_house(file1: str) -> list:
    students = []
    with open(file1) as inputfile:
        reader = csv.DictReader(inputfile)
        for row in reader:
            last, first = row['name'].split(', ')
            house = row['house']
            students.append({'First': first, 'Last': last, 'House': house})
    return students


def write_to_csv(students: list, file2: str):
    with open(file2, 'w', newline='') as outputfile:
        writer = csv.DictWriter(outputfile, fieldnames=[
                                'First', 'Last', 'House'])
        writer.writeheader()
        writer.writerows(students)


def main():
    file1, file2 = get_file_name()
    if check_file_format(file1, file2):
        try:
            students = get_names_and_house(file1)
            write_to_csv(students, file2)
        except FileNotFoundError:
            print('File not found')
            sys.exit()


if __name__ == '__main__':
    main()
