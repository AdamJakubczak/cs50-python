import sys
from tabulate import tabulate
import csv


def main():
    filename = get_file_name()
    if check_file_type(filename):
        menu = get_menu(filename)
        print(tabulate(menu, headers='firstrow', tablefmt='grid'))


def get_menu(filename):
    try:
        with open(filename) as file:
            reader = csv.reader(file)
            menu = [line for line in reader]
            return menu
    except FileNotFoundError:
        print('File does not exist')
        sys.exit()


def get_file_name():
    if len(sys.argv) != 2:
        print('Wrong amount of CLI arguments')
        sys.exit()
    else:
        return sys.argv[1]


def check_file_type(filename: str):
    if filename.endswith('.csv'):
        return True
    else:
        print('Wrong file format')
        sys.exit()


if __name__ == '__main__':
    main()
