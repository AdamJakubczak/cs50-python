import sys


def main():
    file_name = get_file()
    if check_if_python_file(file_name):
        try:
            with open(file_name) as file:
                lines = file.readlines()
                count = count_lines(lines)
        except FileNotFoundError:
            print('File not found')
            sys.exit()
    print(count)


def get_file():
    if len(sys.argv) <= 1:
        print('Not enough command line arguments')
        sys.exit()
    elif len(sys.argv) > 2:
        print('Too many command line arguments')
        sys.exit()
    else:
        return sys.argv[1]


def check_if_python_file(file_name: str):
    if not file_name.endswith('.py'):
        print('Not a python file')
        sys.exit()
    else:
        return True


def count_lines(lines):
    count = 0
    for line in lines:
        if len(line) > 1 and not line.startswith('\n') and not line.startswith('#'):
            count += 1
    return count


main()

integer = input('asdasd: ')
