import seasons
import pytest
from datetime import date


def test_get_date_invalid_data():
    with pytest.raises(SystemExit):
        input_date = 'aaaaaa'
        pytest.MonkeyPatch().setattr('builtins.input', lambda _: input_date)
        seasons.get_date()


def test_get_date_empty_string():
    with pytest.raises(SystemExit):
        input_date = ''
        pytest.MonkeyPatch().setattr('builtins.input', lambda _: input_date)
        seasons.get_date()


def test_get_date_correct_input():
    input_date = '1991-10-23'
    result = date(1991, 10, 23)
    pytest.MonkeyPatch().setattr('builtins.input', lambda _: input_date)
    assert seasons.get_date() == result


def test_get_days_from_birth():
    user_date = date(1991, 10, 23)
    expected_days = (date.today() - user_date).days
    assert seasons.get_days_from_birth(user_date) == expected_days


def test_get_minutes():
    assert seasons.get_minutes(10) == 14400
    assert seasons.get_minutes(0) == 0


def test_numbers_to_words():
    assert seasons.numbers_to_words(1) == 'one'
    assert seasons.numbers_to_words(100) == 'one hundred'
