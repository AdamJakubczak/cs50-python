import pytest
from jar import Jar

def test_init_negative():
    with pytest.raises(ValueError):
        jar = Jar(-1)

def test_init_string():
    with pytest.raises(TypeError):
        jar = Jar('abc')
        
def test_init_default():
    jar = Jar()
    assert jar.capacity == 12
    assert jar.size == 0

def test_init_bigger_capacity():
    jar = Jar(24)
    assert jar.capacity == 24
    assert jar.size == 0

def test_str():
    jar = Jar()
    assert str(jar) == ''
    jar.deposit(1)
    assert str(jar) == '🍪'
    jar.withdraw(1)
    assert str(jar) == ''

def test_deposit():
    jar = Jar()
    assert jar.size == 0
    jar.deposit(1)
    assert jar.size == 1
    jar.deposit(10)
    assert jar.size == 11
    with pytest.raises(ValueError):
        jar.deposit(10)

def test_withdraw():
    jar = Jar()
    assert jar.size == 0
    jar.deposit(10)
    jar.withdraw(1)
    assert jar.size == 9
    jar.withdraw(9)
    assert jar.size == 0
    with pytest.raises(ValueError):
        jar.withdraw(250)
    

