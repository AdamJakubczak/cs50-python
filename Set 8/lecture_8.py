class Worker():
    def __init__(self, name, height) -> None:
        self.name = name
        self.height = height

    def __str__(self):
        return f'{self.name} is {self.height}'

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, height):
        if height != 190:
            raise ValueError('Wrong shiet')
        self._height = height

    @classmethod
    def get(cls):
        name = input('Gibname: ')
        height = int(input('Gibheight: '))
        return cls(name, height)


def main():
    worker = Worker.get()
    print(worker)
    print(worker.height)


main()
