import random


class Hat:
    houses = ['Gryffindor', 'Ravenclaw', 'Hufflepuff', 'Slytherin']
    hous_count = 10
    _hat_age = 30

    @classmethod
    def sort(cls, name):
        house = random.choice(cls.houses)
        return f'{name} in {house}'

    @classmethod
    def print_houses(cls):
        print(cls.houses)

    @property
    def doubled_house_count(self):
        return self.hous_count * 2

    @property
    def hat_age(self):
        return self._hat_age


# print(Hat.sort(input('Gib name: ')))

Hat.print_houses()

hat = Hat()
print(hat.doubled_house_count)
print(hat.hat_age)
