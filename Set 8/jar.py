class Jar:
    def __init__(self, capacity=12):
        self.capacity = capacity
        self.size = 0

    def __str__(self):
        return self.size * '🍪'

    def deposit(self, amount):
        self.size += amount
        if self.size > self.capacity:
            raise ValueError('Too many cookies buddy')

    def withdraw(self, amount):
        self.size -= amount
        if self.size < 0:
            raise ValueError('Fuck no... No mo cookies')

    @property
    def capacity(self):
        return self._capacity
    
    @capacity.setter
    def capacity(self, capacity):
        if capacity < 0:
            raise ValueError('Capcity cannot be negative nyugga')
        self._capacity = capacity
    
    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, size):
        if size > self.capacity:
            raise ValueError('Too many cookies buddy')
        self._size = size
