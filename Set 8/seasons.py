from datetime import date
import sys
import inflect


def main():
    user_date = get_date()
    days_from_birth = get_days_from_birth(user_date)
    minutes = get_minutes(days_from_birth)
    print(f'{numbers_to_words(minutes)} minutes')


def get_date() -> date:
    try:
        user_date = date.fromisoformat(input('Gib date: '))
    except ValueError:
        print('Invalid date')
        sys.exit()
    return user_date


def get_days_from_birth(user_date: date) -> int:
    today = date.today()
    days_from_birth = today - user_date
    return int(days_from_birth.days)


def get_minutes(days_from_birth: date) -> int:
    minutes = days_from_birth * 24 * 60
    return minutes


def numbers_to_words(days_from_birth: date) -> str:
    p = inflect.engine()
    return p.number_to_words(days_from_birth)


if __name__ == "__main__":
    main()
