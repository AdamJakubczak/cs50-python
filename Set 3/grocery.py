ITEMS_LIST = {}

while True:
    try:
        item = input('').lower()
        if item in ITEMS_LIST:
            ITEMS_LIST[item] += 1
        else:
            ITEMS_LIST[item] = 1
    except EOFError:
        break

for item, quantity in ITEMS_LIST.items():
    print(quantity, item.upper())
