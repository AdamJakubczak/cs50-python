MONTHS = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
]

characters_to_replace = [',', '/']


def correct_date(string):
    for char in characters_to_replace:
        string = string.replace(char, ' ')

    month, day, year = string.split()

    if month.isalpha():
        month = MONTHS.index(month)
        month += 1
    if day.isalpha() or year.isalpha() or int(day) > 31 or int(month) > 12:
        raise ValueError

    return int(month), int(day), int(year)


def main():
    while True:
        date = input('Date: ')
        try:
            month, day, year = correct_date(date)
        except ValueError:
            continue
        else:
            print(f'{year}-{month:02}-{day:02}')
            break


main()

# print(correct_date('9/8/1636'))
