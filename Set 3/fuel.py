def get_fraction():
    while True:
        try:
            x, y = input('Fraction: ').split(sep='/')
            if int(x) > int(y):
                continue
            return int(x) / int(y)
        except (ZeroDivisionError, ValueError):
            pass


def main():
    fraction = get_fraction()
    if fraction <= 0.01:
        print('E')
    elif fraction >= 0.99:
        print('F')
    else:
        print("{:.0%}".format(fraction))


main()
